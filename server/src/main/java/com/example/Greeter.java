package com.example;

/**
 * This is a class.
 */
public class Greeter {

  /**
   * This is a constructor.
   */
  public Greeter() {

  }

/**
*@param someone A String name used for the returned string value
*@return A String greeting message
*/
  public final String greet(final String someone) {
    return String.format("Hello, %s!", someone);
  }
}
